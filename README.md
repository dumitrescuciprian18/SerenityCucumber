# Java assured with Serenity and Cucumber and Maven
# IDE used InteliJ
# Installation
git clone https://github.com/CDumitrescu94/SerenityCucumber.git 
Import all dependecies from pom.xml

##Project Endpoints
The project contains GET scenarios, using a github API 
    https://any-api.com/github_com/github_com/docs/_repos_owner_repo_issues/GET
The endpoint is referred to the issues of a repository from github, knowing the User and the Repository name
    "https://api.github.com/repos/CDumitrescu94/AndroidApp/issues"
Using specific parameters on the GET request the response from the server should contain the proper number of issues after the filtering.

repo_issues_filtering
    - Negative Scenario: Filter issues with wrong argument
    - Positive Scenario: Filter issues after state open
    - Positive Scenario: Filter issues after state closed
    - Positive Scenario: Filter issues after state open and label bug
More Scenarios can be impletemented by mixing the query parameters.

## HTML reporting has been done with Maven.
reporting file : /target/site/serenity/index.html
